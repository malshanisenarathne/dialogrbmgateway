package com.dialog.rbmgateway.data.container;

public class RbmQueryParameterInfoContainer {

	/**
	 * The ACCOUNTSTATUS table header.
	 */
	public enum TableColumn_ACCOUNTSTATUS{
		
		/** The column, ACCOUNT_NUM. */
		ACCOUNT_NUM,

		/** The column, ACCOUNT_STATUS. */
		ACCOUNT_STATUS;
	}
	
	/**
	 * The ACCOUNTATTRIBUTES table header.
	 */
	public enum TableColumn_ACCOUNTATTRIBUTES{
		
		/** The column, ACCOUNT_NUM. */
		ACCOUNT_NUM,

		/** The column, DOMAIN_ID. */
		DOMAIN_ID,

		/** The column, NETWORK_STATUS. */
		NETWORK_STATUS;
	}
	
	/**
	 * The CUSTPRODUCTSTATUS table header.
	 */
	public enum TableColumn_CUSTPRODUCTSTATUS{
		
		/** The column, DOMAIN_ID. */
		DOMAIN_ID,
		
		/** The column, CUSTOMER_REF. */
		CUSTOMER_REF,

		/** The column, PRODUCT_STATUS. */
		PRODUCT_STATUS;
	}
	
	/**
	 * The CUSTPRODUCTATTRDETAILS table header.
	 */
	public enum TableColumn_CUSTPRODUCTATTRDETAILS{
		
		/** The column, CUSTOMER_REF. */
		CUSTOMER_REF,

		/** The column, PRODUCT_ATTRIBUTE_SUBID. */
		PRODUCT_ATTRIBUTE_SUBID,
		
		/** The column, ATTRIBUTE_VALUE. */
		ATTRIBUTE_VALUE;
	}
	
	/**
	 * The CUSTPRODUCTATTRDETAILS table header.
	 */
	public enum TableColumn_CUSTPRODUCTDETAILS{
		
		/** The column, CUSTOMER_REF. */
		CUSTOMER_REF,
		
		/** The column, END_DAT. */
		END_DAT;
	}

	/**
	 * The Enum RBM Status.
	 */
	public enum rbmStatus {

		/**Rbm ACCOUNT_STATUS TA*/
		ACCOUNT_STATUS_TA("TA"),
		
		/**Rbm NETWORK_STATUS D*/
		NETWORK_STATUS_D("D"),
		
		/**Rbm NETWORK_STATUS D*/
		PRODUCT_STATUS_TX("Tx");
	
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a RBM Status.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		rbmStatus(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * The Enum Product Attribute SubId.
	 */
	public enum productAttributeSubId {

		/**Id: 9*/
		ID_9("9");
	
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a Product Attribute SubId.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		productAttributeSubId(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * The Enum Attribute Value.
	 */
	public enum attributeValue {

		/**Id: 9*/
		ATTRIBUTE_VALUE_D("D");
	
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a Attribute Value.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		attributeValue(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
}
