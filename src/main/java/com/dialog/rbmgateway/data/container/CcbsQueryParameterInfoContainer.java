package com.dialog.rbmgateway.data.container;

public class CcbsQueryParameterInfoContainer {

	/**
	 * The Enum Status.
	 */
	public enum ccbsStatus {

		/**AccPac Status CCBS_PROCESSED*/
		ACCPAC_STATUS_CCBS_PROCESSED("CCBS_PROCESSED"),
		
		/**Status: D*/
		STATUS_D("D"),
		
		/**Switch Status: C*/
		SWITCH_STATUS_C("C"),
		
		/**Status: C*/
		STATUS_C("C");
		
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a Status.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		ccbsStatus(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * The CAM_ACCPAC_DETAIL_INTERFACE table header.
	 */
	public enum TableColumn_CAM_ACCPAC_DETAIL_INTERFACE{
		
		/** The column, PHONE_NO. */
		PHONE_NO,
		
		/** The column, STATUS. */
		STATUS,
		
		/** The column, ACCPAC_REGISTRATION_NO. */
		ACCPAC_REGISTRATION_NO,
		
		/** The column, IDENTIFICATION_NUMBER. */
		IDENTIFICATION_NUMBER,
		
		/** The column, NAME1. */
		NAME1,
		
		/** The column, NAME2. */
		NAME2,
		
		/** The column, ADDRESS_LINE1. */
		ADDRESS_LINE1,
		
		/** The column, ADDRESS_LINE2. */
		ADDRESS_LINE2;
	}
	
	/**
	 * The Enum serviceId.
	 */
	public enum serviceId {

		/**ServiceId Status VOICE*/
		VOICE("1");
		
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a serviceId.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		serviceId(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * The DYN_1_CONNECTION table header.
	 */
	public enum TableColumn_DYN_1_CONNECTION{
		
		/** The column, MOBILE_NO. */
		MOBILE_NO,
		
		/** The column, STATUS. */
		STATUS,
		
		/** The column, CONNECTION_ID. */
		CONNECTION_ID,
		
		/** The column, SIM. */
		SIM,
		
		/** The column, SERVICE_ID. */
		SERVICE_ID;
	}
	
	/**
	 * The CAM_CONTRACT table header.
	 */
	public enum TableColumn_CAM_CONTRACT{
		
		/** The column, CONTRACT_ID. */
		CONTRACT_ID,
		
		/** The column, SUBSCRIBER_NODE_ID. */
		SUBSCRIBER_NODE_ID,
		
		/** The column, ADDRESS_ID. */
		ADDRESS_ID,
		
		/** The column, CREDIT_TYPE. */
		CREDIT_TYPE,
		
		/** The column, CUSTOMER_CATEGORY. */
		CUSTOMER_CATEGORY,
		
		/** The column, CONTRACT_SUBSIDIARY_TYPE. */
		CONTRACT_SUBSIDIARY_TYPE,
		
		/** The column, STATUS. */
		STATUS;
	}
	
	/**
	 * The CAM_CONTRACT_PACKAGE table header.
	 */
	public enum TableColumn_CAM_CONTRACT_PACKAGE{
		
		/** The column, CONTRACT_PACKAGE_ID. */
		CONTRACT_PACKAGE_ID,
		
		/** The column, CONTRACT_ID. */
		CONTRACT_ID,
		
		/** The column, PACKAGE_ID. */
		PACKAGE_ID,
		
		/** The column, STATUS. */
		STATUS;
	}
	
	/**
	 * The PROV_PENDING_COMMANDS table header.
	 */
	public enum TableColumn_PROV_PENDING_COMMANDS{
		
		/** The column, GROUP_ID. */
		GROUP_ID,
		
		/** The column, PENDING_COMMAND_NO. */
		PENDING_COMMAND_NO;
	}
	
	/**
	 * The Action Code
	 */
	public enum actionCode{
		
		/**Action Code: ProfileAction_D*/
		PROFILE_ACTION_D("ProfileAction_D");
		
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a action Code.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		actionCode(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * The Command Read
	 */
	public enum commandRead{
		
		/**Command Read: NA*/
		COMMAND_READ_NA("NA"),
		
		/**Command Read: N*/
		COMMAND_READ_N("N"),
		
		/**Rbm Command Read: NA*/
		RBM_COMMAND_READ_NA("NA"),
		
		/**Rbm Command Read: N*/
		RBM_COMMAND_READ_N("N"),
		
		/**Rbm Command Read: Y*/
		RBM_COMMAND_READ_Y("Y"),
		
		/**Rbm Command Read: P*/
		RBM_COMMAND_READ_P("P");
		
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a Command Read.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		commandRead(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * enum User
	 *
	 */
	public enum user{
		
		CCBS2("CCBS2");
		
		/** The value. */
		private String value;
		
		/**
		 * Instantiates a user.
		 *
		 * @author MalshaniS
		 * @param value the value
		 */
		user(String value) {
			this.value = value;
		}
		
		/**
		 * Value.
		 *
		 * @author MalshaniS
		 * @return the string
		 */
		public String value() {
			return value;
		}
	}
	
	/**
	 * The CAM_CONNECTION table header.
	 */
	public enum TableColumn_CAM_CONNECTION{
		
		/** The column, STATUS. */
		STATUS,
		
		/** The column, PACKAGE_CONTRACT_ID. */
		PACKAGE_CONTRACT_ID,
		
		/** The column, IMAGE_ID. */
		IMAGE_ID,
		
		/** The column, CONNECTION_ID. */
		CONNECTION_ID;
	}
	
	/**
	 * The PROV_SWITCH_IMAGE table header.
	 */
	public enum TableColumn_PROV_SWITCH_IMAGE{
		
		/** The column, IMAGE_ID. */
		IMAGE_ID,
		
		/** The column, SWITCH_STATUS. */
		SWITCH_STATUS,
		
		/** The column, PENDING_SWITCH_STATUS. */
		PENDING_SWITCH_STATUS;
	}
	
	/**
	 * The OCS_ATTRIBUTE_CHANGES_H table header.
	 */
	public enum TableColumn_OCS_ATTRIBUTE_CHANGES_H{
		
		/** The column, MSISDN. */
		MSISDN,
		
		/** The column, SUCCESS_STATUS. */
		SUCCESS_STATUS,
		
		/** The column, ERROR_CODE. */
		ERROR_CODE;
	}
}
