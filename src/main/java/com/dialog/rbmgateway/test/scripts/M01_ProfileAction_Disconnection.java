package com.dialog.rbmgateway.test.scripts;
import org.testng.annotations.Test;
import com.auxenta.framework.Assert.AuxAssert;
import com.auxenta.framework.db.sql.QueryResult;
import com.auxenta.framework.db.sql.executers.SQLExecuter;
import com.dialog.rbmgateway.base.DialogRbmGatewayBaseTest;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_CAM_CONNECTION;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_CAM_CONTRACT;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_CAM_CONTRACT_PACKAGE;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_DYN_1_CONNECTION;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_OCS_ATTRIBUTE_CHANGES_H;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_PROV_PENDING_COMMANDS;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.TableColumn_PROV_SWITCH_IMAGE;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.actionCode;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.ccbsStatus;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.commandRead;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.serviceId;
import com.dialog.rbmgateway.data.container.CcbsQueryParameterInfoContainer.user;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.TableColumn_ACCOUNTATTRIBUTES;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.TableColumn_ACCOUNTSTATUS;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.TableColumn_CUSTPRODUCTATTRDETAILS;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.TableColumn_CUSTPRODUCTDETAILS;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.TableColumn_CUSTPRODUCTSTATUS;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.attributeValue;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.productAttributeSubId;
import com.dialog.rbmgateway.data.container.RbmQueryParameterInfoContainer.rbmStatus;
import com.dialog.rbmgateway.db.queries.CCBS_SQLQuery;
import com.dialog.rbmgateway.db.queries.RBM_SQLQuery;

/**
 * Test Script: M01_ProfileAction_Disconnection
 * @author MalshaniS
 *
 */
public class M01_ProfileAction_Disconnection extends DialogRbmGatewayBaseTest{
	
	@Test
	public void ProfileAction_Disconnection() throws Exception{
	AuxAssert.init("M01_ProfileAction_Disconnection");
	
	//Get Connected PHONE_NO
	String queryGetConnectionDetails= String.format(CCBS_SQLQuery.GET_CONNECTION_DETAILS_,serviceId.VOICE.value(),"POSTPAID",ccbsStatus.STATUS_C.value(),ccbsStatus.STATUS_C.value(),ccbsStatus.SWITCH_STATUS_C.value());
	QueryResult qsConnectionDetails = SQLExecuter.getQueryResults(queryGetConnectionDetails);
	int rowNo = GetRandomRowNo(qsConnectionDetails.getResultSize());
	String phoneNo = qsConnectionDetails.getValue(TableColumn_DYN_1_CONNECTION.MOBILE_NO.toString(), rowNo);

	//Get User Info
	String queryGetUserInfo = String.format(CCBS_SQLQuery.GET_USER_INFOR_FROM_MOBNO_AND_SERVICE_ID,phoneNo,serviceId.VOICE.value());
	QueryResult qsUserInfo = SQLExecuter.getQueryResults(queryGetUserInfo);
	int rowNoUserInfo = GetRandomRowNo(qsUserInfo.getResultSize());
	String connectionId =qsUserInfo.getValue(TableColumn_DYN_1_CONNECTION.CONNECTION_ID.toString(), rowNoUserInfo);
	String contractId =qsUserInfo.getValue(TableColumn_CAM_CONTRACT.CONTRACT_ID.toString(), rowNoUserInfo);
	String packageId =qsUserInfo.getValue(TableColumn_CAM_CONTRACT_PACKAGE.PACKAGE_ID.toString(), rowNoUserInfo);
	String creditType =qsUserInfo.getValue(TableColumn_CAM_CONTRACT.CREDIT_TYPE.toString(), rowNoUserInfo);
	String customerCategory =qsUserInfo.getValue(TableColumn_CAM_CONTRACT.CUSTOMER_CATEGORY.toString(), rowNoUserInfo);
	String subsidiaryType =qsUserInfo.getValue(TableColumn_CAM_CONTRACT.CONTRACT_SUBSIDIARY_TYPE.toString(), rowNoUserInfo);
	String nodeId =qsUserInfo.getValue(TableColumn_CAM_CONTRACT.SUBSCRIBER_NODE_ID.toString(), rowNoUserInfo);
	
	//Insert Record to OCS_ATTRIBUTE_CHANGES
	String queryInsertOcsAttributeChange= String.format(CCBS_SQLQuery.INSERT_OCS_ATTRIBUTE_CHANGES,phoneNo,contractId,connectionId,actionCode.PROFILE_ACTION_D.value(),"D",commandRead.COMMAND_READ_NA.value(),getSystemDate(),user.CCBS2.value(),generatePendingCommandNo(),customerCategory,"",commandRead.RBM_COMMAND_READ_N.value(),"",creditType,packageId,"C",subsidiaryType,"","");
	SQLExecuter.executeUpdateQuery(queryInsertOcsAttributeChange);
	
/*	//Execute Prov Simulator
	String queryExecuteProvSimulator= String.format(SQLQuery.PROV_SIMULATOR_QUERY,groupId);
	SQLExecuter.executeUpdateQuery(queryExecuteProvSimulator);
*/
	/**CCBS Table Validation*/
	
	//The Table: DYN_1_CONNECTION Validation
	String queryGetDyn1Connection= String.format(CCBS_SQLQuery.GET_DYN_1_CONNECTION,phoneNo);
	QueryResult qsGetDyn1Connection = SQLExecuter.getQueryResults(queryGetDyn1Connection);
	String dyn1Status =qsGetDyn1Connection.getValueFromFirstRow(TableColumn_DYN_1_CONNECTION.STATUS.toString());

	//Assertion_01: Verify STATUS
	AuxAssert.assertEquals(dyn1Status,ccbsStatus.STATUS_D.value(), "Expected DYN_1_CONNECTION table STATUS: "+ccbsStatus.STATUS_D.value()+" is match with the db value: "+dyn1Status);				

	//The Table: CAM_CONNECTION Validation
	String queryGetCamConnection= String.format(CCBS_SQLQuery.GET_CAM_CONNECTION,connectionId);
	QueryResult qsGetCamConnection = SQLExecuter.getQueryResults(queryGetCamConnection);
	String camConnectionStatus =qsGetCamConnection.getValueFromFirstRow(TableColumn_CAM_CONNECTION.STATUS.toString());
	String contractPackageId = qsGetCamConnection.getValueFromFirstRow(TableColumn_CAM_CONNECTION.PACKAGE_CONTRACT_ID.toString());
	String imageId = qsGetCamConnection.getValueFromFirstRow(TableColumn_CAM_CONNECTION.IMAGE_ID.toString());
	
	//Assertion_02: Verify STATUS
	AuxAssert.assertEquals(camConnectionStatus,ccbsStatus.STATUS_D.value(), "Expected CAM_CONNECTION table STATUS: "+ccbsStatus.STATUS_D.value()+" is match with the db value: "+camConnectionStatus);				

	//The Table: CAM_CONTRACT_PACKAGE Validation
	String queryGetCamContractPackage= String.format(CCBS_SQLQuery.GET_CAM_CONTRACT_PACKAGE,contractPackageId);
	QueryResult qsGetCamContractPackage = SQLExecuter.getQueryResults(queryGetCamContractPackage);
	String camConPkgStatus =qsGetCamContractPackage.getValueFromFirstRow(TableColumn_CAM_CONTRACT_PACKAGE.STATUS.toString());

	//Assertion_03: Verify STATUS
	AuxAssert.assertEquals(camConPkgStatus,ccbsStatus.STATUS_D.value(), "Expected CAM_CONTRACT_PACKAGE table STATUS: "+ccbsStatus.STATUS_D.value()+" is match with the db value: "+camConPkgStatus);				

	//The Table: PROV_SWITCH_IMAGE Validation
	String queryGetProvSwitchImage= String.format(CCBS_SQLQuery.GET_PROV_SWITCH_IMAGE,imageId);
	QueryResult qsGetProvSwitchImage = SQLExecuter.getQueryResults(queryGetProvSwitchImage);
	String switchStatus =qsGetProvSwitchImage.getValueFromFirstRow(TableColumn_PROV_SWITCH_IMAGE.SWITCH_STATUS.toString());
	String pendingSwitchStatus =qsGetProvSwitchImage.getValueFromFirstRow(TableColumn_PROV_SWITCH_IMAGE.PENDING_SWITCH_STATUS.toString());
	
	//Assertion_04: Verify STATUS
	AuxAssert.assertEquals(switchStatus,ccbsStatus.STATUS_D.value(), "Expected PROV_SWITCH_IMAGE table SWITCH_STATUS: "+ccbsStatus.STATUS_D.value()+" is match with the db value: "+switchStatus);				

	//Assertion_05: Verify STATUS
	AuxAssert.assertTrue(pendingSwitchStatus.equalsIgnoreCase(""), "Expected PROV_SWITCH_IMAGE table SWITCH_STATUS: "+null+" is match with the db value: "+pendingSwitchStatus);
	
	//The Table: PROV_PENDING_COMMANDS Validation
	String queryGetProvPendingCommand= String.format(CCBS_SQLQuery.GET_PROV_PENDING_COMMANDS,connectionId);
	QueryResult qsGetProvPendingCommand = SQLExecuter.getQueryResults(queryGetProvPendingCommand);
	String dbGroupId =qsGetProvPendingCommand.getValueFromFirstRow(TableColumn_PROV_PENDING_COMMANDS.GROUP_ID.toString());
	
	//Assertion_06: Verify STATUS
	AuxAssert.assertEquals(null, dbGroupId, "Expected PROV_PENDING_COMMANDS table GROUP_ID: "+null+" is match with the db value: "+dbGroupId);

	//The Table: OCS_ATTRIBUTE_CHANGES_H (OACH) Validation
	String queryGetOACH= String.format(CCBS_SQLQuery.GET_OCS_ATTRIBUTE_CHANGES_H,phoneNo);
	QueryResult qsGetOACH = SQLExecuter.getQueryResults(queryGetOACH);
	String dbSuccessStatus =qsGetOACH.getValueFromFirstRow(TableColumn_OCS_ATTRIBUTE_CHANGES_H.SUCCESS_STATUS.toString());
	String dbErrorCode =qsGetOACH.getValueFromFirstRow(TableColumn_OCS_ATTRIBUTE_CHANGES_H.ERROR_CODE.toString());

	//Assertion_07: Verify STATUS
	AuxAssert.assertEquals(dbSuccessStatus, "1", "Expected OCS_ATTRIBUTE_CHANGES_H table SUCCESS_STATUS: "+"1"+" is match with the db value: "+dbSuccessStatus);

	//Assertion_08: Verify ERROR_CODE
	AuxAssert.assertEquals(dbErrorCode, "SUCCESS", "Expected OCS_ATTRIBUTE_CHANGES_H table ERROR_CODE: "+"SUCCESS"+" is match with the db value: "+dbErrorCode);
	
	/**RBM Table Validation*/
	
	//The Table: ACCOUNTSTATUS Validation
	String queryGetAccountStatus= String.format(RBM_SQLQuery.GET_ACCOUNTSTATUS,contractId);
	QueryResult qsGetAccountStatus = SQLExecuter.getQueryResults(queryGetAccountStatus, 1);
	String dbAccountStatus =qsGetAccountStatus.getValueFromFirstRow(TableColumn_ACCOUNTSTATUS.ACCOUNT_STATUS.toString());

	//Assertion_09: Verify ACCOUNT_STATUS
	AuxAssert.assertEquals(dbAccountStatus, rbmStatus.ACCOUNT_STATUS_TA.value(), "Expected ACCOUNTSTATUS table ACCOUNT_STATUS: "+rbmStatus.ACCOUNT_STATUS_TA.value()+" is match with the db value: "+dbAccountStatus);
	
	//The Table: ACCOUNTATTRIBUTES Validation
	String queryGetAccountAttributes= String.format(RBM_SQLQuery.GET_ACCOUNTATTRIBUTES,contractId);
	QueryResult qsGetAccountAttributes = SQLExecuter.getQueryResults(queryGetAccountAttributes, 1);
	String dbNetworkStatus =qsGetAccountAttributes.getValueFromFirstRow(TableColumn_ACCOUNTATTRIBUTES.NETWORK_STATUS.toString());
	String dbDomainId = qsGetAccountAttributes.getValueFromFirstRow(TableColumn_ACCOUNTATTRIBUTES.DOMAIN_ID.toString());
	
	//Assertion_10: Verify NETWORK_STATUS
	AuxAssert.assertEquals(dbNetworkStatus,rbmStatus.NETWORK_STATUS_D.value(), "Expected ACCOUNTATTRIBUTES table NETWORK_STATUS: "+rbmStatus.NETWORK_STATUS_D.value()+" is match with the db value: "+dbNetworkStatus);

	//The Table: CUSTPRODUCTSTATUS Validation
	String queryGetCustProductStatus= String.format(RBM_SQLQuery.GET_CUSTPRODUCTSTATUS,dbDomainId);
	QueryResult qsGetCustProductStatus = SQLExecuter.getQueryResults(queryGetCustProductStatus, 1);
	String dbProductStatus =qsGetCustProductStatus.getValueFromFirstRow(TableColumn_CUSTPRODUCTSTATUS.PRODUCT_STATUS.toString());
	String dbCustomerRef = qsGetCustProductStatus.getValueFromFirstRow(TableColumn_CUSTPRODUCTSTATUS.CUSTOMER_REF.toString());
	
	//Assertion_11: Verify PRODUCT_STATUS
	AuxAssert.assertEquals(dbProductStatus,rbmStatus.PRODUCT_STATUS_TX.value(), "Expected CUSTPRODUCTSTATUS table PRODUCT_STATUS: "+rbmStatus.PRODUCT_STATUS_TX.value()+" is match with the db value: "+dbProductStatus);

	//The Table: CUSTPRODUCTATTRDETAILS Validation
	String queryGetCustProductAttrDetails= String.format(RBM_SQLQuery.GET_CUSTPRODUCTATTRDETAILS,dbCustomerRef);
	QueryResult qsGetCustProductAttrDetails = SQLExecuter.getQueryResults(queryGetCustProductAttrDetails, 1);
	String dbProductAttributeSubId =qsGetCustProductAttrDetails.getValueFromFirstRow(TableColumn_CUSTPRODUCTATTRDETAILS.PRODUCT_ATTRIBUTE_SUBID.toString());
	String dbAttributeValue = qsGetCustProductAttrDetails.getValueFromFirstRow(TableColumn_CUSTPRODUCTATTRDETAILS.ATTRIBUTE_VALUE.toString());
	
	//Assertion_12: Verify PRODUCT_ATTRIBUTE_SUBID
	AuxAssert.assertEquals(dbProductAttributeSubId,productAttributeSubId.ID_9.value(), "Expected CUSTPRODUCTATTRDETAILS table PRODUCT_ATTRIBUTE_SUBID: "+productAttributeSubId.ID_9.value()+" is match with the db value: "+dbProductAttributeSubId);

	//Assertion_13: Verify ATTRIBUTE_VALUE
	AuxAssert.assertEquals(dbAttributeValue,attributeValue.ATTRIBUTE_VALUE_D.value(), "Expected CUSTPRODUCTATTRDETAILS table ATTRIBUTE_VALUE: "+attributeValue.ATTRIBUTE_VALUE_D.value()+" is match with the db value: "+dbAttributeValue);

	//The Table: CUSTPRODUCTDETAILS Validation
	String queryGetCustProductDetails= String.format(RBM_SQLQuery.GET_CUSTPRODUCTDETAILS,dbCustomerRef);
	QueryResult qsGetCustProductDetails = SQLExecuter.getQueryResults(queryGetCustProductDetails, "yyyy-MM-dd", 1);
	String dbEndDate =qsGetCustProductDetails.getValueFromFirstRow(TableColumn_CUSTPRODUCTDETAILS.END_DAT.toString());
	String todayDate = getSystemDate();
	
	//Assertion_14: Verify END_DAT
	AuxAssert.assertEquals(dbEndDate,todayDate, "Expected CUSTPRODUCTDETAILS table END_DAT: "+todayDate+" is match with the db value: "+dbEndDate);

	AuxAssert.finished();
	}
}
