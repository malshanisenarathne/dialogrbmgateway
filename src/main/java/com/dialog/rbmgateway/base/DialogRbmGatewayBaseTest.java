package com.dialog.rbmgateway.base;

import java.util.Random;

import com.auxenta.framework.BasicTestObject;
import com.auxenta.framework.tools.DateTools;

public class DialogRbmGatewayBaseTest{
	
	/**
	 * Instantiates a new dialog rbm gateway base test.
	 */
	public DialogRbmGatewayBaseTest(){
		super();
	}

	/**
	 * Get Random row numbers for select DB data
	 * @return
	 */
	public int GetRandomRowNo(int rawCount){
		Random random=new Random();
		int Low = 0;
		int High = rawCount;
		int rowNo = random.nextInt(High-Low) + Low;
		return rowNo;
	}
	
	/**
	 * Get System Date
	 * @return sysdate
	 */
	public String getSystemDate(){
		String sysdate = DateTools.getTimeStamp("yyyy-MM-dd");
		return sysdate;
	}
	
	/**
	 * Generate Pending Command No
	 * @return pendingCommandNo
	 */
	public int generatePendingCommandNo(){
		Random random=new Random();
		int num = random.nextInt(900000000) + 100000000;
		return num;
	}

}
