package com.dialog.rbmgateway.db.queries;

public interface RBM_SQLQuery {

	/**
	 * %1_ACCOUNT_NUM
	 */
	String GET_ACCOUNTSTATUS = "select * from ACCOUNTSTATUS where ACCOUNT_NUM='%s'";

	/**
	 * %1_ACCOUNT_NUM
	 */
	String GET_ACCOUNTATTRIBUTES = "select * from ACCOUNTATTRIBUTES where ACCOUNT_NUM='%s'";
	
	/**
	 * %1_DOMAIN_ID
	 */
	String GET_CUSTPRODUCTSTATUS = "select * from CUSTPRODUCTSTATUS where DOMAIN_ID='%s'";

	/**
	 * %1_CUSTOMER_REF
	 */
	String GET_CUSTPRODUCTATTRDETAILS = "select * from CUSTPRODUCTATTRDETAILS where CUSTOMER_REF='%s'";
	
	/**
	 * %1_CUSTOMER_REF
	 */
	String GET_CUSTOMER = "select * from CUSTOMER where CUSTOMER_REF='%s'";
	
	/**
	 * %1_CUSTOMER_REF
	 */
	String GET_CUSTPRODUCTDETAILS = "select * from CUSTPRODUCTDETAILS where CUSTOMER_REF='%s' order by START_DAT desc";
}
